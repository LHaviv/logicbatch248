﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            // Only for input
            Console.WriteLine("Input the length of the matrix");
            int length = int.Parse(Console.ReadLine());

            int[,] array2D = new int[length, length];

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + (i + 1) + " set of numbers");
                string numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToIntArray(numbers);
                if (array.Length != length)
                {
                    Console.WriteLine("Wrong numbers, try again");
                    i = 0;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                } 
            }
            Console.WriteLine();

        }
    }
}
