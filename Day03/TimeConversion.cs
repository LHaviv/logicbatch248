﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConversion
    {
        public static void Resolve()
        {
            Console.WriteLine("Input the time");
            string time = Console.ReadLine();

            DateTime dateTime = Convert.ToDateTime(time);

            Console.WriteLine(dateTime.ToString("HH:mm:ss"));
        }
    }
}
