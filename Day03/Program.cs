﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Solve me first");
                        SolveMeFirst.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Time Conversion");
                        TimeConversion.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Simple array sum");
                        SimpleArraySum.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Diagonal difference");
                        DiagonalDifference.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
            }
        }
    }
}
