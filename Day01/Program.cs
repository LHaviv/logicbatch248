﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input nomor Soal");
            int nomorSoal = int.Parse(Console.ReadLine());

            if (nomorSoal == 1)
            {
                // Soal penghitungan BMI

                Console.WriteLine("Masukan tinggi dalam satuan meter");
                double tinggi = double.Parse(Console.ReadLine());

                Console.WriteLine("Masukan berat dalam satuan meter");
                double berat = double.Parse(Console.ReadLine());

                double BMI = berat / (tinggi * tinggi);

                if (BMI < 18.5)
                {
                    Console.WriteLine("Underweight");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("Overweight");
                }
                else
                {
                    Console.WriteLine("Normal");
                }

            }
            else if (nomorSoal == 2)
            {
                // Soal deret satu dimensi LMS

                Console.WriteLine("Masukan panjang angka");
                int length = int.Parse(Console.ReadLine());

            //Soal 01

                int[] angkaGanjilArray = new int[length];
                int angkaGanjil = 1;

                // memasukan value
                for (int i = 0; i < length; i++)
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil += 2;
                }

                // cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.WriteLine(angkaGanjilArray[i]);
                }

                Console.WriteLine();

            //Soal2
                int angkaGenap = 2;
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGenap + " ");
                    angkaGenap += 2;
                }
                Console.WriteLine();

                //Soal 12
                int[] array12 = new int[length];
                int angkaGanjil12 = 1;

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 0)
                    {
                        if (i < length / 2)
                        {
                            array12[i] = angkaGanjil12;
                            array12[length - 1 - i] = angkaGanjil12;

                            angkaGanjil12 += 2;
                        }
                    }
                    else
                    {
                        if (i <= length / 2)
                        {
                            array12[i] = angkaGanjil12;
                            array12[length - 1 - i] = angkaGanjil12;

                            angkaGanjil12 += 2;
                        }
                    }
                }

                for (int i = 0; i < length; i++)
                {
                    Console.Write(array12[i] + " ");
                }

            }
            else
            {
                Console.WriteLine("Nomor soal tidak ditemukan");
            }

            Console.ReadKey();
        }
    }
}
